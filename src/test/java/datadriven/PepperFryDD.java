package datadriven;

import org.testng.annotations.Test;

import utils.HelperFunctions;
import utils.MoreHelperFunctions;
import utils.DataReaders;


import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class PepperFryDD {


	WebDriver driver;

	@BeforeTest
	public void beforeTest() {
		
		driver = HelperFunctions.createAppropriateDriver("chrome");
		

	}


	@Test(dataProvider = "exceldp")
	public void f(String searchKeyword) {
		
		System.out.println("Search keyword is :" + searchKeyword);

		driver.get("http://www.pepperfry.com/");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"searchButton\"]")));
		
		// enter keyword in the search
		
		By searchLocator = By.xpath("//*[@id='search']");
		WebElement searchElement = driver.findElement(searchLocator);
		searchElement.clear();
		searchElement.sendKeys(searchKeyword);
		//click on the search button
		driver.findElement(By.xpath("//input[@id='searchButton']")).click();
		
		MoreHelperFunctions.wait(2); // this is for debugging purposes - we can take a screenshot too
		

		
	}

		


	//static values data provider for testing purposes
	@DataProvider
	public Object[][] dp() {
		return new Object[][] {
			new Object[] {"sofa" },
			new Object[] {"bed" },
		};
	}

	//excel sheet  data provider for testing purposes
	@DataProvider
	public Object[][] exceldp() throws IOException {
		
		System.out.println("Reading from excel file : src/test/java/datadriven/pepperfry.xlsx");
		return DataReaders.getExcelDataUsingPoi("src/test/java/datadriven/pepperfry.xlsx","Sheet1");
	}

	
	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
